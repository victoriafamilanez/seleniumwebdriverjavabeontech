$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("Search.feature");
formatter.feature({
  "comments": [
    {
      "line": 1,
      "value": "# language: en"
    }
  ],
  "line": 3,
  "name": "Search on Google",
  "description": "As a user, I want to access the Google search API\r\nAnd I want to search for an item",
  "id": "search-on-google",
  "keyword": "Feature"
});
formatter.scenario({
  "line": 7,
  "name": "Valid Search for YouTube",
  "description": "",
  "id": "search-on-google;valid-search-for-youtube",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 8,
  "name": "I navigate to the Google website",
  "keyword": "Given "
});
formatter.step({
  "line": 9,
  "name": "I perform a search for \"YouTube\"",
  "keyword": "When "
});
formatter.step({
  "line": 10,
  "name": "I should see a list of related results",
  "keyword": "Then "
});
formatter.match({
  "location": "SearchResultStep.navigateToGoogleWebsite()"
});
formatter.result({
  "duration": 3215646700,
  "status": "passed"
});
formatter.match({
  "location": "ValidSearchStep.I_insert_the_youtube_string()"
});
formatter.result({
  "duration": 1012267500,
  "status": "passed"
});
formatter.match({
  "location": "ValidSearchStep.I_should_see_a_list_of_related_results()"
});
formatter.result({
  "duration": 1762804900,
  "status": "passed"
});
formatter.scenario({
  "line": 12,
  "name": "Search Result Navigation to Facebook",
  "description": "",
  "id": "search-on-google;search-result-navigation-to-facebook",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 13,
  "name": "I navigate to the Google website",
  "keyword": "Given "
});
formatter.step({
  "line": 14,
  "name": "I perform a search for \"Facebook\"",
  "keyword": "When "
});
formatter.step({
  "line": 15,
  "name": "click on the Facebook result",
  "keyword": "And "
});
formatter.step({
  "line": 16,
  "name": "I should be redirected to the Facebook page",
  "keyword": "Then "
});
formatter.match({
  "location": "SearchResultStep.navigateToGoogleWebsite()"
});
formatter.result({
  "duration": 2945292600,
  "status": "passed"
});
formatter.match({
  "location": "SearchResultStep.performFacebookSearch()"
});
formatter.result({
  "duration": 276512300,
  "status": "passed"
});
formatter.match({
  "location": "SearchResultStep.clickOnFacebookResult()"
});
formatter.result({
  "duration": 3028322300,
  "status": "passed"
});
formatter.match({
  "location": "SearchResultStep.shouldBeRedirectedToFacebookPage()"
});
formatter.result({
  "duration": 7148600,
  "status": "passed"
});
});