
# BEON.Tech Test Automation Project

This project employs Selenium WebDriver with Java for test automation. Below is an overview of the project structure, dependencies, configurations, installation steps, and instructions on running the project from the command line.

##Test Design and Assumptions

This test automation project focuses on validating the search functionality on Google and the subsequent navigation to specific search results, such as YouTube and Facebook.

**Test Scenarios**

1. **Valid Search for YouTube:**
    - *Description:* This scenario verifies that a user can perform a valid search for "YouTube" on the Google website and observes a list of related results.
    - *Assumptions:*
        - The Google homepage is accessible and loads successfully.
        - Searching for "YouTube" yields relevant results.

2. **Search Result Navigation to Facebook:**
    - *Description:* This scenario ensures that a user can navigate to the Facebook page by searching for it on the Google website.
    - *Assumptions:*
        - The Google homepage is accessible and loads successfully.
        - Searching for "Facebook" returns the official Facebook page as one of the results.
        - Clicking on the Facebook result redirects the user to the official Facebook page.

**Test Automation Framework**

The project uses Selenium WebDriver with Java and Cucumber for test automation. The WebDriverManager library is employed to handle browser driver management automatically.

**Project Structure**

The project structure follows a modular design:
- `pages`: Contains page objects representing different pages on the application.
- `runner`: Manages the test execution and browser configurations.
- `steps`: Defines step definitions for Cucumber scenarios.
- `support`: Includes utility methods and shared functionalities.

**How to Run the Tests**

To execute the automated tests, follow the steps outlined in the "How to Run the Project from the Command Line" section in the README file.

Please note that these details provide insights into the purpose of the tests, the assumptions made, and the overall structure of the test automation framework.


### RESOURCES Folder
- **features:** BDD scenarios are written here.

## Dependencies

### Cucumber
- **cucumber-junit:** Executes Cucumber scenarios with JUnit.
- **cucumber-java:** Provides support for translating BDD.

### Selenium WebDriver
- **selenium-java:** WebDriver dependency for browser automation.

### Cluecumber Report Plugin
- **cluecumber-report-plugin:** Generates HTML reports from Cucumber test results.

### Surefire Plugin
- **maven-surefire-plugin:** Configures the execution of tests and handles failures.

### WebDriverManager
- **webdrivermanager:** Automates the management of browser drivers.

## Configuration

### Maven Compiler Plugin
- The Maven Compiler Plugin is configured to use Java 8 for compilation.

### Cluecumber Report Plugin
- This plugin generates HTML reports from Cucumber test results during the post-integration-test phase.

### Surefire Plugin
- The Maven Surefire Plugin is configured to ignore test failures and continue the build.

### WebDriverManager
- Automates the management of browser drivers, ensuring compatibility.

## How to Install

1. **Check Java Version:**
    - Open a command prompt (CMD) and check the installed Java version:
      ```bash
      java --version
      ```
    - If Java is not installed, download the [Java SE Development Kit](https://www.oracle.com/java/technologies/javase-jdk15-downloads.html) and install it.

2. **Download IDE IntelliJ:**
    - Download and install [IntelliJ IDEA](https://www.jetbrains.com/idea/).

3. **Download Browser Drivers:**
    - Download [chromedriver](https://sites.google.com/chromium.org/driver/) and [geckodriver](https://github.com/mozilla/geckodriver/releases).

4. **Install Maven:**
    1. **Download Maven:**
        - Visit the [official Apache Maven website](https://maven.apache.org/download.cgi) and download the latest version.
    2. **Extract the Archive:**
        - Extract the downloaded ZIP file to a directory of your choice.
    3. **Set Up Environment Variables (Windows):**
        - Add the path of the Maven bin directory to your Path.
        - Go to system settings → Advanced system settings → Environment Variables.
        - Under "System Variables," select the "Path" variable and click "Edit."
        - Add a new entry with the path to the Maven `bin` directory.
    4. **Verify the Installation:**
        - Open CMD and execute the following command:
          ```bash
          mvn -v
          ```
        - This should display the Maven version and other information.
    5. **Configure in IntelliJ IDEA (Optional):**
        - If you are using IntelliJ IDEA, go to File → Settings → Build, Execution, Deployment → Build Tools → Maven.
        - Specify the path to the Maven directory you extracted.

## How to Run the Project from the Command Line

1. **Navigate to the Project Directory:**
    - Open a command prompt (CMD) and change the directory to where your project is located:
      ```bash
      cd path/to/your/project
      ```

2. **Compile and Run the Tests:**
    - Use Maven to compile and run the tests. Execute the following command:
      ```bash
      mvn clean test
      ```

3. **Generate and View the Cluecumber Report:**
    - After the tests have run, generate the Cluecumber HTML report with the following command:
      ```bash
      mvn test -Dtest=**/*RunCucumberTest cluecumber-report:reporting
      ```
    - Open the generated report by navigating to the `target/generated-report/index.html` file in a web browser.

4. **Configure Browsers for WebDriverManager (Optional):**
    - If you haven't configured WebDriverManager in your code to automatically download browser drivers, ensure the required browsers (e.g., Chrome, Firefox) are installed on your machine.

Now you should be able to run your project and view the test results and reports from the command line.

## How to View Reports

On the folder "formated-report," find the HTML file "index.html," then right-click to open it in a browser.

---