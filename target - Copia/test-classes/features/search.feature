# language: en

Feature: Search an article on the website
  As a user of a HealthCare website
  I want to search for a specific article
  To read about it
  Scenario: Search and article
    Given that I am on the HealthCare website
    When I click on the search bar
    And type the name of the article
    And click enter
    Then I will be able to see the article I searched for