# language: en


  Feature: Receive the discount coupon from HealthCare
    As a user of HealthCare platform
    I want to receive a discount coupon
    to purchase a ebook at a reduced price

    Scenario: View discount code
      Given that I am on the HealthCare website
      When I fill in my email
      And click on the "Get Coupon" button
      Then I see the discount code





