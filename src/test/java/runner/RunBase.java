package runner;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;

public class RunBase {

    // Static WebDriver instance to be shared across the test run
    static WebDriver driver;

    // Enum representing supported browsers
    public enum Browser {CHROME, FIREFOX}

    /**
     * Get the WebDriver instance. If the instance is not created yet,
     * create it using the default browser (CHROME).
     *
     * @return The WebDriver instance.
     */
    public static WebDriver getDriver() {
        if (driver == null) {
            return getDriver(Browser.CHROME);
        } else {
            return driver;
        }
    }

    /**
     * Get the WebDriver instance based on the specified browser type.
     * If an instance already exists, quit it before creating a new one.
     *
     * @param browser The desired browser type.
     * @return The WebDriver instance.
     */
    public static WebDriver getDriver(Browser browser) {
        if (driver != null) {
            driver.quit();
        }
        switch (browser) {
            case CHROME:
                // Configure Chrome options, allowing remote origins
                ChromeOptions options = new ChromeOptions();
                options.addArguments("--remote-allow-origins=*");
                // Initialize ChromeDriver with the configured options
                driver = new ChromeDriver(options);
                break;
            case FIREFOX:
                // Initialize FirefoxDriver
                driver = new FirefoxDriver();
                break;
            default:
                throw new IllegalArgumentException("Please, insert a valid browser");
        }
        return driver;
    }
}