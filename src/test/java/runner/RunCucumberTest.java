package runner;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.AfterClass;
import org.junit.runner.RunWith;


@RunWith(Cucumber.class)
@CucumberOptions(
        // Define the plugins for generating reports in JSON and HTML formats
        plugin = {"json:target/reports/cucumberReport.json", "html:target/reports/"},
        // Specify the location of feature files
        features = "src/test/resources/features",
        // Exclude scenarios tagged with @ignore
        tags = ("~@ignore"),
        // Specify the package where step definitions are located
        glue = "steps"
)

public class RunCucumberTest extends RunBase {


    // Teardown method to run after the test class
    @AfterClass
    public static void stop (){
        //Quit the WebDriver to close the browser
        driver.quit();

    }


}
