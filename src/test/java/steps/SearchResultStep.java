package steps;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import pages.HomePage;
import runner.RunBase;
import runner.RunCucumberTest;

public class SearchResultStep extends RunCucumberTest {

    // Instantiate the HomePage class to access its methods
    HomePage homepage = new HomePage();

    // Selectors and expected URL for Facebook
    public static final String FACEBOOK_LOGO_SELECTOR = "a[href*='facebook.com'] > div > span";
    public static final String EXPECTED_FACEBOOK_URL_BASE = "https://www.facebook.com/campaign/landing.php?";

    // Step to navigate to the Google website
    @Given("^I navigate to the Google website$")
    public void navigateToGoogleWebsite(){
        getDriver(RunBase.Browser.CHROME);
        homepage.navigateToGoogle();
    }

    // Step to perform a search for "Facebook"
    @When("^I perform a search for \"Facebook\"")
    public void performFacebookSearch()  {
        homepage.insertStringOnSearchBar("facebook");
    }
    // Step to click on the Facebook result
    @When("^click on the Facebook result$")
    public void clickOnFacebookResult() {
        WebElement facebookLogo = getDriver().findElement(By.cssSelector(FACEBOOK_LOGO_SELECTOR));
        // Assert that the Facebook logo is displayed before clicking
        Assert.assertTrue("Facebook logo is not displayed", facebookLogo.isDisplayed());
        facebookLogo.click();
    }

    // Step to verify redirection to the Facebook page
    @Then("^I should be redirected to the Facebook page$")
    public void shouldBeRedirectedToFacebookPage() {
        // Get the current URL
        String currentUrl = getDriver().getCurrentUrl();

        // Assert that the current URL contains the expected base URL
        Assert.assertTrue("The current URL does not contain the expected base URL: " + EXPECTED_FACEBOOK_URL_BASE, currentUrl.contains(EXPECTED_FACEBOOK_URL_BASE));
    }
}