package steps;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;
import org.openqa.selenium.By;
import pages.HomePage;
import runner.RunCucumberTest;


// Instantiate the HomePage class to access its methods
public class ValidSearchStep extends RunCucumberTest {
    // Create an instance of the HomePage class to access its methods
    HomePage homepage = new HomePage();

    // XPath for locating the YouTube logo on the search results page
    public static final String YOUTUBE_LOGO = "//span[text()=\"YouTube\"]";

    // Step definition for searching the youtube
    @When("^I perform a search for \"YouTube\"")
    public void I_insert_the_youtube_string()  {
        // Navigate to the Google website
        homepage.navigateToGoogle();
        // Call the method to search youtube
        homepage.insertStringOnSearchBar("youtube");

    }

    // Step definition for verifying successful search
    @Then("^I should see a list of related results$")
    public void I_should_see_a_list_of_related_results()  {
        // Get the text of the YouTube logo on the search results page
        String youtubelogo = getDriver().findElement(By.xpath(YOUTUBE_LOGO)).getText();
        // Assert that the YouTube logo is displayed with the expected text
        Assert.assertEquals("The YouTube logo is incorrect", "YouTube", youtubelogo);

    }
}
