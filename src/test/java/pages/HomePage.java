package pages;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import runner.RunCucumberTest;
import support.Utilis;


public class HomePage extends RunCucumberTest {

    // URLs and locators as constants
    public static final String URL = "https://www.google.com.br/";
    public static final String GOOGLE_LOGO = "img[alt='Google']";
    public static final String SEARCH_BAR = "//textarea[@type=\"search\"]";



    // Method to navigate to the Google website
    public void navigateToGoogle() {
        // Open the Google website
        getDriver().get(URL);
        // Wait for the Google logo to be displayed before proceeding
        Utilis.waitForElementToBeShown(By.cssSelector(GOOGLE_LOGO),10);
        
    }


    // Method to search an item on google
    public void insertStringOnSearchBar (String searchString){
        // Locate the search bar field
        WebElement searchBarField = getDriver().findElement(By.xpath(SEARCH_BAR));
        // Assert that the search bar field is displayed before interacting with it
        Assert.assertTrue("Search bar field is not displayed", searchBarField.isDisplayed());
        // Enter the string to search
        searchBarField.sendKeys(searchString);
        // Press ENTER key to initiate the search
        searchBarField.sendKeys(Keys.ENTER);

    }


}
