package support;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import runner.RunCucumberTest;

import java.time.Duration;


public class Utilis extends RunCucumberTest{

    /**
     * Waits for the specified element to be shown on the web page and be clickable.
     *
     * @param element  The By object representing the target element.
     * @param duration The maximum duration to wait for the element, in seconds.
     */

    public void waitForElementToBeClickable(By element, int duration){
        // Create a WebDriverWait instance with the specified duration
       WebDriverWait wait = new WebDriverWait(getDriver(), Duration.ofSeconds(duration));
        // Wait until the element becomes clickable
       wait.until(ExpectedConditions.elementToBeClickable(element));
    }

    public static void waitForElementToBeShown(By element, int duration){
        // Create a WebDriverWait instance with the specified duration
        WebDriverWait wait = new WebDriverWait(getDriver(), Duration.ofSeconds(duration));
        // Wait until the element becomes visible
        wait.until(ExpectedConditions.visibilityOfElementLocated(element));

    }
}
