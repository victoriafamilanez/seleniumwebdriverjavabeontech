# language: en

Feature: Search on Google
  As a user, I want to access the Google search API
  And I want to search for an item

  Scenario: Valid Search for YouTube
    Given I navigate to the Google website
    When I perform a search for "YouTube"
    Then I should see a list of related results

  Scenario: Search Result Navigation to Facebook
    Given I navigate to the Google website
    When I perform a search for "Facebook"
    And click on the Facebook result
    Then I should be redirected to the Facebook page